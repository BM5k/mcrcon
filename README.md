# Run [mcrcon](https://github.com/Tiiffi/mcrcon) in a container

## Usage

```
docker run -it --rm -e RCON_HOST=HOSTNAME -e RCON_PWD=PASSWORD registry.gitlab.com/bm5k/mcrcon
```
