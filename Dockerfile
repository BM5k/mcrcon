FROM alpine:latest

ENV RCON_HOST=127.0.0.1
ENV RCON_PORT=25575
ENV RCON_PWD=""

ADD https://github.com/Tiiffi/mcrcon/releases/download/v0.0.5/mcrcon-0.0.5-linux-x86.tar.gz /src/
RUN cd /src && \
    tar xzf /src/mcrcon-0.0.5-linux-x86.tar.gz mcrcon && \
    mv mcrcon /usr/local/bin/mcrcon && \
    rm /src/mcrcon-0.0.5-linux-x86.tar.gz

CMD mcrcon -H $RCON_HOST -P $RCON_PORT -p $RCON_PWD -t
